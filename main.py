import argparse
import json
import os
import subprocess
from configparser import ConfigParser
from typing import Tuple, Optional


__version__ = "0.1.0"
PUBLIC_ADDRESS_SCANNER = "https://ip.me"
NFT_BIN = "/usr/sbin/nft"

MANDATORY_CONFIG = [
    "last_known_addresses",
    "table"
]


def main():
    parser = argparse.ArgumentParser(description="Dynamic firewall updater")
    parser.add_argument("config", help="The configuration file")

    args = parser.parse_args()
    if not os.path.isfile(args.config):
        raise IOError(f"Unable to find config file: {args.config}")
    config = ConfigParser()
    config.read(args.config)
    global_config = config["global"]

    for item in MANDATORY_CONFIG:
        if item not in global_config:
            raise ValueError(f"'{item}' not found in provided configuration file")

    last_known_file = global_config["last_known_addresses"]
    last_ipv4, last_ipv6 = get_last_known_addresses(last_known_file)
    ipv4, ipv6 = get_current_ip_addresses()

    # Add subnets if specified
    if ipv4 is not None and "ipv4_subnet" in global_config:
        subnet = global_config["ipv4_subnet"]
        ipv4 = f"{ipv4}/{subnet}"
    if ipv6 is not None and "ipv6_subnet" in global_config:
        subnet = global_config["ipv6_subnet"]
        ipv6 = f"{ipv6}/{subnet}"

    table = global_config["table"]
    ipv4_set = global_config.get("ipv4_set", None)
    ipv6_set = global_config.get("ipv6_set", None)

    update_firewall(table, ipv4_set, ipv6_set, ipv4, last_ipv4, ipv6, last_ipv6)

    # If the IP addresses have changed, update the last known addresses
    if ipv4 != last_ipv4 or ipv6 != last_ipv6:
        save_last_known_addresses(last_known_file, ipv4, ipv6)


def update_firewall(table: str, ipv4_set: Optional[str], ipv6_set: Optional[str],
                    ipv4: Optional[str], last_ipv4: Optional[str],
                    ipv6: Optional[str], last_ipv6: Optional[str]):
    if ipv4_set is not None:
        # If the IPv4 has changed, remove the old one
        if last_ipv4 is not None and ipv4 != last_ipv4:
            _run(NFT_BIN, "delete", "element", "ip", table, ipv4_set, "{" + last_ipv4 + "}", ignore_errors=True)
            print(f"Old IPv4 '{last_ipv4}' removed from firewall whitelist")

        # If the IPv4 is not in the whitelist, add it
        if ipv4 is not None and not _test(NFT_BIN, "get", "element", "ip", table, ipv4_set, "{" + ipv4 + "}"):
            _run(NFT_BIN, "add", "element", "ip", table, ipv4_set, "{" + ipv4 + "}")
            print(f"IPv4 '{ipv4}' added to firewall whitelist")

    if ipv6_set is not None:
        # If the IPv6 has changed, remove the old one
        if last_ipv6 is not None and ipv6 != last_ipv6:
            _run(NFT_BIN, "delete", "element", "ip6", table, ipv6_set, "{" + last_ipv6 + "}", ignore_errors=True)
            print(f"Old IPv6 '{last_ipv6}' removed from firewall whitelist")

        # If the IPv4 is not in the whitelist, add it
        if ipv6 is not None and not _test(NFT_BIN, "get", "element", "ip6", table, ipv6_set, "{" + ipv6 + "}"):
            _run(NFT_BIN, "add", "element", "ip6", table, ipv6_set, "{" + ipv6 + "}")
            print(f"IPv6 '{ipv6}' added to firewall whitelist")


def get_last_known_addresses(file: str) -> Tuple[Optional[str], Optional[str]]:
    if not os.path.isfile(file):
        return None, None

    with open(file, "r") as f:
        data = json.loads(f.read())

    ipv4 = data["ipv4"] if "ipv4" in data else None
    ipv6 = data["ipv6"] if "ipv6" in data else None
    return ipv4, ipv6


def save_last_known_addresses(file: str, ipv4: Optional[str], ipv6: Optional[str]):
    data = {}
    if ipv4 is not None:
        data["ipv4"] = ipv4
    if ipv6 is not None:
        data["ipv6"] = ipv6

    with open(file, "w") as f:
        f.write(json.dumps(data, indent=4))


def get_current_ip_addresses() -> Tuple[Optional[str], Optional[str]]:
    try:
        ipv4 = _get("curl", "-4", PUBLIC_ADDRESS_SCANNER)
    except ChildProcessError:
        # IPv4 is not supported
        ipv4 = None

    try:
        ipv6 = _get("curl", "-6", PUBLIC_ADDRESS_SCANNER)
    except ChildProcessError:
        # IPv6 is not supported
        ipv6 = None

    return ipv4, ipv6


def _run(*command: str, ignore_errors: bool = False):
    result = subprocess.run(command)
    if result.returncode != 0 and not ignore_errors:
        raise ChildProcessError(f"Unable to run command {command}: error code {result.returncode}")


def _get(*command: str) -> str:
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if result.returncode != 0:
        raise ChildProcessError(f"Unable to perform HTTP request: error code {result.returncode}")
    return result.stdout.decode("utf8").replace("\n", "")


def _test(*command: str) -> bool:
    result = subprocess.run(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return result.returncode == 0


if __name__ == "__main__":
    main()
