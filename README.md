# Dynamic firewall updater

This tool can be used to keep an `nftable` firewall up to date with a machine's public IPv4/IPv6.

The public addresses are dynamically added to/removed from [nftable sets](https://wiki.nftables.org/wiki-nftables/index.php/Sets).

The tool uses [ip.me](https://ip.me) to detect to machine's public IP.

## Usage

```
usage: main.py [-h] config

Dynamic firewall updater  

positional arguments:     
  config      The configuration file

options:
  -h, --help  show this help message and exit
```

## Configuration file

The configuration file determines how the tool works. See [config.ini.example](./config.ini.example) for a list of properties that can be configured in this file.
